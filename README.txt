A simple python script to read graphics files from old Westwood Studio games, and convert to PNG images.

Has only been tested on "Lands of Lore 1" ("Throne of Chaos"). May require some changes to work on other Westwood games.

You will need data files from the original game. This tool is only intended for making it easier to study and admire the amazing pixel art from the era.

Most technical information needed was found on https://moddingwiki.shikadi.net/

Usage:
    python3 -m venv env
    env/bin/pip install pypng  # if you want to do more than unpack PAK
    env/bin/python lolrip.py -h

You probably need to start by unpacking some PAK files (use the "pak" command) and then you can start converting the CPS and WSA files found inside to PNG. WSA files without palette require you to supply a CPS file with the proper palette (otherwise there will just be a greyscale palette, to hopefully make it possible to make anything out). SHP files don't really work (yet). See the script for more details.
