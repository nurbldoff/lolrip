import array
import struct
import sys
from typing import NamedTuple

try:
    import png
except ImportError:
    print("No PyPNG found; can't write image files")


def read_pak_header(f):
    "PAK Header consists of offsets and filenames."
    file_offsets = []
    while True:
        # Read a file offset
        offset = struct.unpack("<L", f.read(4))[0]
        name = b""
        while True:
            # Read a file name, NULL terminated
            character = f.read(1)
            if character == b"\0":
                break
            name += character
        if name:
            file_offsets.append((name.decode(), offset))
        else:
            # Filename is null; gives us the size of the file.
            last_size = offset
            break
    files = {}
    for (name, offset1), (_, offset2) in zip(file_offsets, file_offsets[1:]):
        size = offset2 - offset1
        files[name] = (offset1, size)
    last_name, last_offset = file_offsets[-1]
    files[last_name] = (last_offset, last_size - last_offset)
    return files


class CPSHeader(NamedTuple):
    size: int
    compression: int
    orig_size: int
    palette_size: int


def read_cps_header(f):
    return CPSHeader(*struct.unpack("<HHLH", f.read(2 + 2 + 4 + 2)))


def read_palette(f, palette_size):
    values = struct.unpack("<" + "B" * palette_size, f.read(palette_size))

    # Palette is stored as 6 bit per channel, need to convert to 8 bit
    return [
        ((r * 255) // 63,
         (g * 255) // 63,
         (b * 255) // 63)
        for r, g, b in zip(values[::3], values[1::3], values[2::3])
    ]


def decompress_lcw(source, size):
    """
    Decompress an image with Westwood "LCW" compression (AKA "format 80")
    Implementation following https://moddingwiki.shikadi.net/wiki/Westwood_LCW
    """
    dest = array.array("B", [0] * size)
    sp = 0  # source pointer
    dp = 0  # destination pointer
    rel = source[sp]
    if rel == 0:
        sp += 1

    while True:
        com = source[sp]
        sp += 1
        b7 = com >> 7
        if b7 == 0:  # Copy (command 2)
            count = ((com & 0x7F) >> 4) + 3
            posit = ((com & 0x0F) << 8) + source[sp]
            sp += 1
            posit = dp - posit
            for i in range(posit, posit + count):
                dest[dp] = dest[i]
                dp += 1
        elif b7 == 1:
            b6 = (com & 0x40) >> 6
            if b6 == 0:
                count = com & 0x3F
                if count == 0:
                    break  # EOF
                for i in range(1, count + 1):
                    dest[dp] = source[sp]
                    dp += 1
                    sp += 1
            elif b6 == 1:
                count = com & 0x3F
                if count < 0x3E:  # Large copy (command 3)
                    count += 3
                    if rel == 0:
                        posit = dp - struct.unpack("<H", source[sp:sp+2])[0]
                    else:
                        posit = struct.unpack("<H", source[sp:sp+2])[0]
                    sp += 2
                    for i in range(posit, posit + count):
                        dest[dp] = dest[i]
                        dp += 1
                elif count == 0x3F:  # Very large copy (command 5)
                    count = struct.unpack("<H", source[sp:sp+2])[0]
                    if rel == 0:
                        posit = dp - struct.unpack("<H", source[sp+2:sp+4])[0]
                    else:
                        posit = struct.unpack("<H", source[sp+2:sp+4])[0]
                    sp += 4
                    for i in range(posit, posit + count):
                        dest[dp] = dest[i]
                        dp += 1
                else:
                    count = struct.unpack("<H", source[sp:sp+2])[0]
                    sp += 2
                    b = source[sp]
                    sp += 1
                    for i in range(count):
                        dest[dp] = b
                        dp += 1
    return dest


def apply_xor(src, dst):

    """
    XOR encoding is used in WSA files. AKA "format 40".
    """

    sp = 0  # source pointer
    dp = 0  # destination pointer

    while True:
        cmd = src[sp]
        sp += 1
        if cmd == 0:
            # XOR with value
            count = src[sp]
            sp += 1
            value = src[sp]
            sp += 1
            for _ in range(count):
                dst[dp] ^= value
                dp += 1
        elif (cmd & 0x80) == 0:
            # XOR with string
            count = cmd
            for _ in range(count):
                dst[dp] ^= src[sp]
                dp += 1
                sp += 1
        elif (cmd != 0x80):
            # skip
            count = cmd & 0x7F
            dp += count
        else:
            cmd = src[sp]
            sp += 1
            cmd += src[sp] << 8
            sp += 1

            if cmd == 0:
                break   # 0x800000 = Exit

            if (cmd & 0x8000) == 0:
                # Skip
                dp += cmd
            elif (cmd & 0x4000) == 0:
                # XOR with string
                count = cmd & 0x3FFF
                for _ in range(count):
                    dst[dp] ^= src[sp]
                    dp += 1
                    sp += 1
            else:
                count = cmd & 0x3FFF
                value = src[sp]
                sp += 1
                for _ in range(count):
                    dst[dp] = value
                    dp += 1
    return dst


class WSAHeader(NamedTuple):
    n_frames: int
    xpos: int
    ypos: int
    width: int
    height: int
    buffer_size: int
    flags: int


def read_wsa_header(f):
    header = WSAHeader(*struct.unpack("<HHHHHHH", f.read(2 * 7)))
    print(header)
    return header


def handle_pak(args):
    """
    Westwood PAK archive format.
    Stores multiple files without compressing them, kind of like "tar".
    https://moddingwiki.shikadi.net/wiki/PAK_Format_(Westwood)
    """
    with open(args.pakfile, "rb") as pakf:
        file_positions = read_pak_header(pakf)
        print(file_positions)
        for name, (offset, size) in file_positions.items():
            with open(os.path.join(args.destdir, name), "wb") as outf:
                pakf.seek(offset)
                outf.write(pakf.read(size))


def handle_cps(args):
    """
    Westwood CPS image compression format.
    Stores a 256 color (6 bit per channel) image using simple compression.
    https://moddingwiki.shikadi.net/wiki/Westwood_CPS_Format
    """
    with open(args.cpsfile, "rb") as cpsf:
        header = read_cps_header(cpsf)
        print(header)
        if header.palette_size != 0:
            palette = read_palette(cpsf, header.palette_size)
        else:
            palette = []
        img = decompress_lcw(cpsf.read(), header.orig_size)
    if header.orig_size == 64000:
        # TODO any other way to figure out the size?
        w = 320
        h = 200
    else:
        w = args.width
        h = args.height

    writer = png.Writer(w, h, bitdepth=8, alpha=False, palette=palette)
    with open(args.dest, "wb") as f:
        writer.write_array(f, img)


def handle_wsa(args):
    """
    Westwood Studios Animation format
    Stores a series of animation frames in an efficient way. Used for cutscenes.
    See https://moddingwiki.shikadi.net/wiki/Westwood_WSA_Format"
    """
    header = read_wsa_header(args.wsafile)
    n_offsets = header.n_frames + 2
    frame_offsets = struct.unpack("<" + "L" * n_offsets,
                                  args.wsafile.read(4 * n_offsets))
    buffer_size = header.width * header.height
    if args.initial_frame:
        with open(args.initial_frame, "rb") as cpsf:
            cps_header = read_cps_header(cpsf)
            if cps_header.palette_size != 0:
                palette = read_palette(cpsf, cps_header.palette_size)
            else:
                raise RuntimeError("Found no palette in initial frame")
            if frame_offsets[0] == 0:
                pass
                # dest = decompress_lcw(cpsf.read(), cps_header.orig_size)
                # TODO Probably need to crop this initial frame to size of the frames
                # Haven't needed this feature yet though.
        dest = array.array("B", [0] * buffer_size)
    else:
        palette = None
        dest = array.array("B", [0] * buffer_size)
    if not palette:
        if header.flags == 1:
            palette_size = 768
            palette = read_palette(args.wsafile, palette_size)
        else:
            palette_size = 0
            # Fake grayscale palette just to show something
            palette = [(i, i, i) for i in range(256)]
    if args.show_palette:
        show_palette(palette)
    for i in range(0, header.n_frames):
        offset = frame_offsets[i]
        next_offset = frame_offsets[i+1]
        size = next_offset - offset
        # The XOR data is compressed, using the usual LCW method
        source = decompress_lcw(args.wsafile.read(size), header.buffer_size + 37)
        dest = apply_xor(source, dest)
        writer = png.Writer(header.width, header.height, bitdepth=8, alpha=False, palette=palette)
        outfile = f"{args.basename}{i}.png"
        print(outfile)
        with open(f"{args.basename}{i}.png", "wb") as f:
            writer.write_array(f, dest)


def show_palette(pal):
    print("Palette size", len(pal))
    for y in range(8):
        for x in range(32):
            r, g, b = pal[y * 32 + x]
            show_pixel(r, g, b)
        print("\u001b[0m")


def show_pixel(r, g, b, char="  "):
    sys.stdout.write(f"\u001b[48;2;{r};{g};{b}m{char}")


class SHPFrameHeader(NamedTuple):
    flags: int
    slices: int
    width: int
    height: int
    filesize: int
    zero_compressed_size: int


def handle_shp(args):
    """
    Westwood SHP format, apparently used for tiles, sprites etc.
    """
    cps_header = read_cps_header(args.shpfile)
    print(cps_header)
    if cps_header.palette_size != 0:
        palette = read_palette(args.shpfile, cps_header.palette_size)
    else:
        palette = []
    data = decompress_lcw(args.shpfile.read(), cps_header.orig_size)
    args.shpfile.close()

    data_ptr = 0
    n_frames = struct.unpack("<H", data[:2])[0]
    data_ptr += 2
    print(n_frames)
    n_offsets = n_frames + 1
    frame_offsets = struct.unpack("<" + "L" * n_offsets, data[data_ptr:data_ptr + n_offsets * 4])
    print(frame_offsets)
    #data = decompress_lcw(args.shpfile.read(), cps_header.orig_size)
    # TODO...


if __name__ == "__main__":
    import argparse
    import os

    parser = argparse.ArgumentParser(
        description="Decodes graphics from old Westwood Studios games like 'Lands of Lore'.")
    subparsers = parser.add_subparsers()

    pak_parser = subparsers.add_parser('pak', help='Unpack a PAK file, containing other files.')
    pak_parser.add_argument("pakfile", type=str)
    pak_parser.add_argument("-d", "--destdir", type=str, default=".")
    pak_parser.set_defaults(func=handle_pak)

    cps_parser = subparsers.add_parser('cps', help='Convert a CPS file into a PNG image.')
    cps_parser.add_argument("cpsfile", type=str)
    cps_parser.add_argument("-d", "--dest", type=str)
    cps_parser.add_argument("--width", type=int, default=0)
    cps_parser.add_argument("--height", type=int, default=0)
    cps_parser.set_defaults(func=handle_cps)

    wsa_parser = subparsers.add_parser('wsa', help='Convert a WSA animation into several PNG images.')
    wsa_parser.add_argument("wsafile", type=argparse.FileType("br"))
    wsa_parser.add_argument("-b", "--basename", type=str)
    wsa_parser.add_argument("-p", "--show-palette", action="store_true")
    wsa_parser.add_argument("-i", "--initial-frame", type=str, help="A CPS file with the correct palette.")
    wsa_parser.set_defaults(func=handle_wsa)

    shp_parser = subparsers.add_parser('shp', help='Convert a SHP tile file into PNG images.')
    shp_parser.add_argument("shpfile", type=argparse.FileType("br"))
    shp_parser.add_argument("-b", "--basename", type=str)
    shp_parser.add_argument("-p", "--show-palette", action="store_true")
    shp_parser.add_argument("-i", "--initial-frame", type=str)
    shp_parser.set_defaults(func=handle_shp)

    args = parser.parse_args()
    if hasattr(args, "func"):
        args.func(args)
    else:
        sys.exit("You need to provide a command, try -h")
